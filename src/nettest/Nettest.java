package nettest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Random;

public class Nettest {

	public static void main(String[] args) {

		paramParser(args);

		System.out.println("Network Tester Started");

		if(serverMode){
			startServer();
		}
		else{
			startClient();
		}
	}

	private static void paramParser(String[] args) {
		int i = 0;

		while(i < args.length){
			switch(args[i]){
			case "-s":
				serverMode = true;
				i++;
				break;
			case "-m":
				i++;
				if(args[i].toUpperCase().equals("UDP")){
					proto = "UDP";
				} else if(args[i].toUpperCase().equals("TCP")){
					proto = "TCP";
				}
				break;
				
			case "-l":
				i++;
				dataSize = Integer.parseInt(args[i]);
				i++;
				break;
				
			case "-p":
				i++;
				portNumber = Integer.parseInt(args[i]);
				i++;
				break;
			case "-n":
				i++;
				number = Integer.parseInt(args[i]);
				i++;
				break;
			default:
				target = args[i];
				i++;

			}
		}

	}

	private static void startServer(){
		while (true){
			if(proto.equals("TCP")){
				serverTCP();
			}else if(proto.equals("UDP")){
				serverUDP();
			}

			System.out.println("Restarting server");
		}

	}

	private static void startClient(){

		if(proto.equals("TCP")){
			clientTCP();
		}else if(proto.equals("UDP")){
			clientUDP();
		}

	}

	private static void serverTCP(){

		System.out.println("Listening on port " + proto + " " + portNumber);
		try (
				ServerSocket serverSocket = new ServerSocket(portNumber);
				Socket clientSocket = serverSocket.accept();
				PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);                   
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				)
				{
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				out.println(inputLine);
			}

				} catch (IOException e) {
					System.out.println("Error when trying to listen on port " + portNumber);
					System.out.println(e.getMessage());
				}

	}


	private static void clientTCP(){

		try (
				Socket echoSocket = new Socket(target, portNumber);
				PrintWriter out =
						new PrintWriter(echoSocket.getOutputStream(), true);
				BufferedReader in =
						new BufferedReader(
								new InputStreamReader(echoSocket.getInputStream()));
				) {
			String transferData;
			long start;
			long elapsedTime;
			float time;
			DecimalFormat df = new DecimalFormat("0.000");

			for (int i=0; i<number; i++){
				transferData = getRandomText(dataSize);
				start = System.nanoTime();
				//System.out.println("Sending " + transferData);
				out.println(transferData);
				//System.out.println("echo: " + in.readLine());
				elapsedTime = System.nanoTime() - start;
				time = elapsedTime/1000000F;
				System.out.println("Reply from " + target + ": byte=" + dataSize + " time=" + df.format(time) + "ms speed=" + df.format(dataSize*8/time)  + " Kbps");
			}
		} catch (UnknownHostException e) {
			System.err.println("Could not find host " + target + ". Please check the name and try again.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Could not connect to host " + target + ". Make sure server is running on the target.");
			System.exit(1);
		} 


	}	

	private static void serverUDP() {
		byte[] receive_data = new byte[dataSize];

		int recv_port;

		DatagramSocket server_socket = null;
		try {
			server_socket = new DatagramSocket(portNumber);
			System.out.println("Listening on port " + proto + " " + portNumber);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		while(true)
		{

			try {
				DatagramPacket receive_packet = new DatagramPacket(receive_data,receive_data.length);                                  
				server_socket.receive(receive_packet);
				String data = new String(receive_packet.getData(),0,0,receive_packet.getLength());
				InetAddress IPAddress = receive_packet.getAddress(); 
				recv_port = receive_packet.getPort();
				System.out.println("Reply to " + IPAddress);
				server_socket.send(receive_packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				//server_socket.close();
			}
		}
	}






	private static void clientUDP() {
		byte[] send_data = new byte[dataSize];
		byte[] receive_data = new byte[dataSize];
		DatagramSocket client_socket = null;
		InetAddress IPAddress = null;
		try {
			client_socket = new DatagramSocket();
			IPAddress = InetAddress.getByName(target);

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long start;
		long elapsedTime;
		float time;
		DecimalFormat df = new DecimalFormat("0.000");

		for (int i=0; i<number; i++){


			try {

				String data = getRandomText(dataSize);
				send_data = data.getBytes();
				DatagramPacket send_packet = new DatagramPacket(send_data,send_data.length,IPAddress, portNumber);
				DatagramPacket receive_packet = new DatagramPacket(receive_data,receive_data.length);
				start = System.nanoTime();
				client_socket.send(send_packet);
				client_socket.receive(receive_packet);
				elapsedTime = System.nanoTime() - start;
				String receivedData = new String(receive_packet.getData(),0,0,receive_packet.getLength());
				time = elapsedTime/1000000F;
				System.out.println("Reply from " + target + ": byte=" + dataSize + " time=" + df.format(time) + "ms speed=" + df.format(dataSize*8/time)  + " Kbps");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}       
		client_socket.close();
	}	

	private static String getRandomText(int dataSize) {

		int charsetSize =  randomCharset.length();

		StringBuilder sb = new StringBuilder( dataSize );
		for( int i = 0; i < dataSize; i++ ) 
			sb.append( randomCharset.charAt( rnd.nextInt(charsetSize) ) );

		return sb.toString();
	}
	static Random rnd = new Random();
	static final String randomCharset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	private static String proto = "TCP";
	private static String target = "127.0.0.1";
	private static int portNumber = 8000;
	private static boolean serverMode = false;
	private static int number = 4;
	private static int dataSize = 32;




}